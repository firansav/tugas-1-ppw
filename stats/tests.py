from django.test import TestCase, Client
from django.urls import resolve
from .views import stats
from updatestatus.models import Status
from profile.views import name
from addfriend.models import AddFriend

# Create your tests here.

class StatsUnitTest(TestCase):
    def test_stats_url_is_exist(self):
        response = Client().get('/stats/')
        self.assertEqual(response.status_code,200)
    def test_stats_using_stats_func(self):
        found = resolve('/stats/')
        self.assertEqual(found.func, stats)
    def test_stats_post_feed_count(self):
        feedCount = Status.objects.all().count()
        response = Client().get('/stats/')
        html_response = response.content.decode('utf8')

        self.assertIn('Feed: '+str(feedCount),html_response)
    def test_stats_post_friend_count(self):
        friendCount = AddFriend.objects.all().count()
        response = Client().get('/stats/')
        html_response = response.content.decode('utf8')

        self.assertIn('Friends: '+str(friendCount),html_response)
    def test_latest_post_shown(self):
        new_post = Status.objects.create(description = 'Title')
        latest_post = Status.objects.create(description = 'Latest')
        
        lastCount = Status.objects.all().count()
        lastFeed = Status.objects.all()[lastCount-1]
        response = Client().get('/stats/')
        html_response = response.content.decode('utf8')

        self.assertIn(lastFeed.description, html_response)
