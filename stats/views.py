from django.shortcuts import render
from addfriend.models import AddFriend
from updatestatus.models import Status
from profile.views import name

# Create your views here.
def stats(request):
    status = Status.objects.all()
    feedCount = Status.objects.all().count()
    response = {'author':'Kelompok 5C','friendz':AddFriend.objects.all().count(),
                'feed':feedCount,'name':name, 'app':'Dashboard'};
    if(feedCount > 0):
        response['status'] = status[feedCount - 1]
    return render(request, 'stats.html', response)
