from django.test import TestCase, Client
from django.urls import resolve
from .views import profile

class profileUnitTest(TestCase):

	def test_profile_url_is_exist(self):
		response = Client().get('/profile/')
		self.assertEqual(response.status_code, 200)

	def test_profile_using_index_func(self):
		found = resolve('/profile/')
		self.assertEqual(found.func, profile)