from django.db import models

# Create your models here.
class ProfileData(models.Model):
	name = models.CharField(max_length=250)
	Birthday = models.CharField(max_length=20)
	Gender = models.CharField(max_length=200)
	expertise = models.TextField(max_length=200)
	Description = models.CharField(max_length=200)
	Email = models.CharField(max_length=200)