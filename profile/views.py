from django.shortcuts import render
from datetime import datetime, date
from .models import ProfileData

author = 'Kelompok 5C'
name = 'Hepzibah Smith' #TODO implement your name here
birth_date = date(1995,4,23) #TODO implement your birthday
birthdate = birth_date.strftime('%d %B %Y')
gender = 'Male' #TODO implement your gender
email = 'hello@smith.com' #TODO implement your email
desc_profile = "Full Stack Developer"
#TODO implement your expertise minimal 3
expert = ["Back End", "Front End", "UI/UX"]

def profile(request):
	# response = {}
	# response['author'] = "Kelompok 5C"
	# response = {'prof_dict': prof_dict}
	# return render(request, 'profile.html', response)
	profil = ProfileData(name = name, Birthday = birthdate, Gender= gender, Email = email, Description=desc_profile, expertise=expert)
	response = {'name' : profil.name, 'Birthday' : profil.Birthday, 'Gender': profil.Gender, 'Expertise': profil.expertise, 
	'Description' : profil.Description, 'Email': profil.Email, 'author':author,'app':'Profile'}
	html = 'profile.html'
	return render(request, html, response)