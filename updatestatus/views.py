from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Status
from .forms import Status_Form
from profile.views import name

# Create your views here.
response = {}

def index(request):
    response['author'] = "Kelompok 5C"
    status = reversed(Status.objects.all())
    response['status'] = status
    html = 'updatestatus/updatestatus.html'
    response['status_form'] = Status_Form
    response['app'] = "Update Status"
    response['name'] = name
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        status = Status(description=response['description'])
        status.save()
        return HttpResponseRedirect('/updatestatus/')
    else:
        return HttpResponseRedirect('/updatestatus/')

def delete_status(request, id_status):
    status = Status.objects.get(pk = id_status)
    status.delete()
    return HttpResponseRedirect('/updatestatus/')