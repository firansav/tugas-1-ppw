[![pipeline status](https://gitlab.com/firansav/tugas-1-ppw/badges/master/pipeline.svg)](https://gitlab.com/firansav/tugas-1-ppw/commits/master)

[![coverage report](https://gitlab.com/firansav/tugas-1-ppw/badges/master/coverage.svg)](https://gitlab.com/firansav/tugas-1-ppw/commits/master)

# Tugas 1 PPW

Pengembangan​ ​Aplikasi​ ​Web​ ​dengan​ ​TDD,​ ​Django,​ ​Models​ ​,​ ​HTML,​ ​CSS

## Kelompok 5 Kelas C:
1. Firandra Savitri (1606829043) - Update Status
2. Nabilah Badriyah Gelshirani
3. Tatag Aziz Prawiro
4. Winston Chandra

## Link HerokuApp:

https://tp1k5c.herokuapp.com/