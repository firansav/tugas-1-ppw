from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import AddFriend
from .forms import AddFriend_Form

# Create your views here.
response = {}

def index(request):
    response['author'] = "Kelompok 5C"
    addfriend = AddFriend.objects.all()
    response['addfriend'] = addfriend
    html = 'addfriend/addfriend.html'
    response['addfriend_form'] = AddFriend_Form
    response['app'] = 'Add Friend'
    return render(request, html, response)
    
def add_friend(request):
    addfriend = AddFriend_Form(request.POST or None)
    if(request.method == 'POST' and addfriend.is_valid()):
        response['name'] = request.POST['name']
        response['link'] = request.POST['link']
        addfriendd = AddFriend(name=response['name'],link=response['link'])
        addfriendd.save()
        return HttpResponseRedirect('/addfriend/')
    else:
        return HttpResponseRedirect('/addfriend/')