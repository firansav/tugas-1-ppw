from django import forms

class AddFriend_Form(forms.Form):
	error_messages = {
        'required': 'This field is required.',
    }
	name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Masukan nama...'
    }
	link_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Masukan link, contoh : https://kelompok5C.herokuapp.com'
    }
	name = forms.CharField(label='Nama ', required=True, max_length=40, widget=forms.TextInput(attrs=name_attrs),error_messages = error_messages)
	link = forms.URLField(label='URL  ', required=True, max_length=40, widget=forms.TextInput(attrs=link_attrs),error_messages = error_messages)