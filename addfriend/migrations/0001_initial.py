# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-08 12:50
from __future__ import unicode_literals

import addfriend.models
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AddFriend',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40)),
                ('link', models.CharField(max_length=40)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
