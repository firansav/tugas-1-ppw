from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from .views import index, add_friend
from .models import AddFriend
from .forms import AddFriend_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class AddFriendUnitTest(TestCase):
	def test_add_friend_url_is_exist(self):
		response = Client().get('/addfriend/')
		self.assertEqual(response.status_code,200)
	def test_add_friend_using_index_func(self):
		found = resolve('/addfriend/')
		self.assertEqual(found.func, index)
	def test_model_can_add_new_friend(self):
		#Creating a new activity
		new_activity = AddFriend.objects.create(name='Winston Chan',link = 'http://winston.herokupp.com')

		#Retrieving all available activity
		counting_all_available_friend = AddFriend.objects.all().count()
		self.assertEqual(counting_all_available_friend,1)
	def test_form_validation_for_blank_items(self):
		addfriendform = AddFriend_Form(data={'name': '', 'link': ''})
		self.assertFalse(addfriendform.is_valid())
		self.assertEqual(
			addfriendform.errors['name'],["This field is required."]
		)
		self.assertEqual(	addfriendform.errors['link'],["This field is required."]
		)
	def test_addfriend_success_and_render_the_result(self):
		test = 'Anonymous'
		test1 = 'herokuapp.com'
		response_post = Client().post('/addfriend/add_friend', {'name': test, 'link': test1})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/addfriend/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_addfriend_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/addfriend/add_friend', {'name': '', 'link': ''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/addfriend/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)		
	
		
class AddFriendFunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(AddFriendFunctionalTest, self).setUp()		
	def tearDown(self):
		self.selenium.quit()
		super(AddFriendFunctionalTest, self).tearDown()
	def test_input_addfriend(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get('http://127.0.0.1:8000/addfriend/')
		# find the form element
		name = selenium.find_element_by_id('id_name')
		link = selenium.find_element_by_id('id_link')

		submit = selenium.find_element_by_id('submit')

		# Fill the form with data
		name.send_keys('Winston')
		link.send_keys('https://winston.herokuapp.com')

		# submitting the form
		submit.send_keys(Keys.RETURN)